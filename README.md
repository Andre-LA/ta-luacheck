# ta-luacheck

Very basic Luacheck integration to TextAdept

## How to install

Install [luarocks][install-luarocks-url]

Install [luacheck][install-luacheck-url]  

Place the "ta-luacheck" directory to your "modules/lua/" directory (by default, "~./textadept/modules/lua")

In your textadept init.lua (by default, "~./textadept/init.lua")

```lua
events.connect(events.LEXER_LOADED, function(lexer) 
  if lexer == 'lua' then
    _M.ta_luacheck = require('lua.ta-luacheck') -- this loads ta-luacheck
  end
end)
```

And done!

## How to use

In menubar, Click in Tools --> Run Luacheck

This will run luacheck in the current buffer and print the results in [Message Buffer]

This is how I use it:

![ta-luacheck screenshot](screenshot-ta-luacheck.png)

I split view and associate it with [Message Buffer]

[install-luarocks-url]: https://github.com/luarocks/luarocks/wiki/Download
[install-luacheck-url]: https://luarocks.org/modules/mpeterv/luacheck
